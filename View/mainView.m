function varargout = mainView(varargin)
% MAINVIEW MATLAB code for mainView.fig
%      MAINVIEW, by itself, creates a new MAINVIEW or raises the existing
%      singleton*.
%
%      H = MAINVIEW returns the handle to a new MAINVIEW or the handle to
%      the existing singleton*.
%
%      MAINVIEW('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAINVIEW.M with the given input arguments.
%
%      MAINVIEW('Property','Value',...) creates a new MAINVIEW or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before mainView_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to mainView_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help mainView

% Last Modified by GUIDE v2.5 06-Feb-2015 12:58:29

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @mainView_OpeningFcn, ...
                   'gui_OutputFcn',  @mainView_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before mainView is made visible.
function mainView_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to mainView (see VARARGIN)

% Choose default command line output for mainView
handles.output = hObject;
% Choose default command line output for measures
handles.output = hObject;

% get handle to the controller
for i = 1:2:length(varargin)
    switch varargin{i}
        case 'controller'
            handles.controller = varargin{i+1};
        otherwise
            error('unknown input')
    end
end

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes mainView wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = mainView_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in loadTrain.
function loadTrain_Callback(hObject, eventdata, handles)
% hObject    handle to loadTrain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.controller.loadTrainSet;


function trainPath_Callback(hObject, eventdata, handles)
% hObject    handle to trainPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of trainPath as text
%        str2double(get(hObject,'String')) returns contents of trainPath as a double
newPath = get(handles.trainPath, 'string');
handles.controller.changeTrainSetPath(newPath);

% --- Executes during object creation, after setting all properties.
function trainPath_CreateFcn(hObject, eventdata, handles)
% hObject    handle to trainPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in testSetLoad.
function testSetLoad_Callback(hObject, eventdata, handles)
% hObject    handle to testSetLoad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.controller.loadTestSet;

function testSetPath_Callback(hObject, eventdata, handles)
% hObject    handle to testSetPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of testSetPath as text
%        str2double(get(hObject,'String')) returns contents of testSetPath as a double
newPath = get(handles.testSetPath, 'string');
handles.controller.changeTestSetPath(newPath);


% --- Executes during object creation, after setting all properties.
function testSetPath_CreateFcn(hObject, eventdata, handles)
% hObject    handle to testSetPath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function dataSetIndex_Callback(hObject, eventdata, handles)
% hObject    handle to dataSetIndex (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of dataSetIndex as text
%        str2double(get(hObject,'String')) returns contents of dataSetIndex as a double
index = str2num(get(handles.dataSetIndex, 'String'));
handles.controller.updateIndex(index);

% --- Executes during object creation, after setting all properties.
function dataSetIndex_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dataSetIndex (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in visualize.
function visualize_Callback(hObject, eventdata, handles)
% hObject    handle to visualize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.controller.visualizeImage(handles);

% --- Executes on button press in applyReduction.
function applyReduction_Callback(hObject, eventdata, handles)
% hObject    handle to applyReduction (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
newDimensions = str2num(get(handles.newDimensions, 'String'));
trainCheckbox = get(handles.trainSetCheckbox, 'Value');
testCheckbox = get(handles.testSetCheckbox, 'Value');
handles.controller.applyReduction(newDimensions, trainCheckbox, testCheckbox);


function newDimensions_Callback(hObject, eventdata, handles)
% hObject    handle to newDimensions (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of newDimensions as text
%        str2double(get(hObject,'String')) returns contents of newDimensions as a double


% --- Executes during object creation, after setting all properties.
function newDimensions_CreateFcn(hObject, eventdata, handles)
% hObject    handle to newDimensions (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in configureClassifier.
function configureClassifier_Callback(hObject, eventdata, handles)
% hObject    handle to configureClassifier (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in executeClassifier.
function executeClassifier_Callback(hObject, eventdata, handles)
% hObject    handle to executeClassifier (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
knn = get(handles.knnSelector, 'Value');
bayesian = get(handles.bayesianSelector, 'Value');
mlp = get(handles.mlpSelector, 'Value');
mlp = cell2mat(mlp(end));
som = get(handles.somSelector, 'Value');
if knn
    selectorType = 'knn';
elseif bayesian
    selectorType = 'bayesian';
elseif mlp
    selectorType = 'mlp';
elseif som
    selectorType = 'som';
end
handles.controller.executeClassifier(selectorType);

% --- Executes on button press in showResults.
function showResults_Callback(hObject, eventdata, handles)
% hObject    handle to showResults (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.controller.showResults;

% --- Executes on button press in confusionMatrixCheckbox.
function confusionMatrixCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to confusionMatrixCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of confusionMatrixCheckbox


% --- Executes on button press in somMapCheckbox.
function somMapCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to somMapCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of somMapCheckbox


% --- Executes on button press in trainButton.
function trainButton_Callback(hObject, eventdata, handles)
% hObject    handle to trainButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes when selected object is changed in dataSetSelector.
function dataSetSelector_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in dataSetSelector 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)


% --- Executes when selected object is changed in reductionSelector.
function reductionSelector_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in reductionSelector 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)


% --- Executes when selected object is changed in classifierSelector.
function classifierSelector_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in classifierSelector 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in showReconstructedImage.
function showReconstructedImage_Callback(hObject, eventdata, handles)
% hObject    handle to showReconstructedImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.controller.showReconstructedImage(handles);

% --- Executes on button press in trainSetCheckbox.
function trainSetCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to trainSetCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of trainSetCheckbox


% --- Executes on button press in testSetCheckbox.
function testSetCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to testSetCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of testSetCheckbox


% --- Executes on button press in numberOfErrorsCheckbox.
function numberOfErrorsCheckbox_Callback(hObject, eventdata, handles)
% hObject    handle to numberOfErrorsCheckbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of numberOfErrorsCheckbox



function trainLabel_Callback(hObject, eventdata, handles)
% hObject    handle to trainLabel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of trainLabel as text
%        str2double(get(hObject,'String')) returns contents of trainLabel as a double


% --- Executes during object creation, after setting all properties.
function trainLabel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to trainLabel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function classifiedAs_Callback(hObject, eventdata, handles)
% hObject    handle to classifiedAs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of classifiedAs as text
%        str2double(get(hObject,'String')) returns contents of classifiedAs as a double


% --- Executes during object creation, after setting all properties.
function classifiedAs_CreateFcn(hObject, eventdata, handles)
% hObject    handle to classifiedAs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
