classdef myView < handle
    %MYVIEW Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        %GUI's
        gui;
        mainGui;
        knnConfigurationGui;
        bayesianConfigurationGui;
        mlpConfigurationGui;
        somConfigurationGui;
        %Controller
        controller;
        %Model
        model;
    end
    
    methods
        %Constructor
        function obj = myView(controller)
            obj.controller = controller;
            obj.model = controller.model;
            obj.mainGui = mainView('controller', obj.controller);
            
            %Set up
            
            
            %Add listeners           
            addlistener(obj.model, 'trainSetSize', 'PostSet', ...
                        @(src, evnt)myView.handlePropEvents(obj, src, evnt));
            addlistener(obj.model, 'testSetSize', 'PostSet', ...
                @(src, evnt)myView.handlePropEvents(obj, src, evnt));
            addlistener(obj.model, 'dataSetOriginalDimensions', 'PostSet', ...
                @(src, evnt)myView.handlePropEvents(obj, src, evnt));
            addlistener(obj.model, 'reconstructedTrainData', 'PostSet', ...
                @(src, evnt)myView.handlePropEvents(obj, src, evnt));
            addlistener(obj.model, 'reconstructedTestData', 'PostSet', ...
                @(src, evnt)myView.handlePropEvents(obj, src, evnt));
            addlistener(obj.model, 'dataSetIndex', 'PostSet', ...
                @(src, evnt)myView.handlePropEvents(obj, src, evnt));
        end
    end
    
    methods (Static)
        function handlePropEvents(obj,src,evnt)
            evntobj = evnt.AffectedObject;
            handles = guidata(obj.mainGui);
            switch src.Name                
                case 'trainSetSize'
                    visualizeSelected = get(handles.visualizeTrainSelector, 'Value');
                    if visualizeSelected == 1
                        set(handles.dataSetIndex,'String', num2str(1));
                        set(handles.dataSetSize,'String', num2str(evntobj.trainSetSize));
                    end
                case 'testSetSize'
                    visualizeSelected = get(handles.visualizeTestSelector, 'Value');
                    if visualizeSelected == 1
                        set(handles.dataSetIndex,'String', num2str(1));
                        set(handles.dataSetSize,'String', num2str(evntobj.testSetSize));
                    end
                case 'dataSetOriginalDimensions'
                    originalDimensions = evntobj.dataSetOriginalDimensions;
                    set(handles.newDimensions, 'String', num2str(originalDimensions));
                    set(handles.originalDimensions, 'String', num2str(originalDimensions));
                case 'reconstructedTrainData'
                    set(handles.applyReduction, 'String', 'Apply reduction');
                    set(handles.applyReduction, 'Enable', 'on');
                case 'reconstructedTestData'
                    set(handles.applyReduction, 'String', 'Apply reduction');
                    set(handles.applyReduction, 'Enable', 'on');
                case 'dataSetIndex'
                    index = str2num(get(handles.dataSetIndex, 'String'));
                    if index < handles.controller.model.trainSetSize;
                        set(handles.trainLabel, 'String', num2str(handles.controller.model.trainLabel(index)));
                    end
                    index = str2num(get(handles.dataSetIndex, 'String'));
                    if index < length(handles.controller.model.classificationResults);
                        set(handles.classifiedAs, 'String', num2str(handles.controller.model.classificationLabel(index)));
                    end
                otherwise
                    error('Unknown units');
            end
        end
    end    
end

