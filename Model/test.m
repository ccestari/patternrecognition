clear all
close all;
clc
load Trainnumbers.mat
img = Trainnumbers.image;
%Calculating covariance
covariance = cov(img', 1);
[transMatc, diagonal] = eig(covariance);
[sortedDiag, order] = sort(diag(diagonal), 'descend');
n = 10;
principalComponents = transMatc(:, order(1:n*n));

%Calculate the coeficients for every image into every principal component
%For just 1 in this case
projection = [];
reconstructedImage = zeros(784, 1);
for i = 1 : n*n
    projection(end + 1) = dot(img(:, 1), principalComponents(:, i)'); %every column of principal component is an eigen vector
    reconstructedImage(:, 1) = (projection(end) * principalComponents(:, i) / (norm(principalComponents(:, i)) ^ 2)) + reconstructedImage(:, 1);
end
image(reshape(reconstructedImage(:, 1), 28, 28));

pca = featureModule;
[reconstructedImage2, pcaCoordinates, mse] = pca.reduceDimension(img, n, 1);

for i = 1 : size(reconstructedImage2, 1)
    assert(reconstructedImage2(i, 1) == reconstructedImage(i, 1));
end

for i = 1 : size(pcaCoordinates, 1)
    assert(pcaCoordinates(i, 1) == projection(1, i));
end