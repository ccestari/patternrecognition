classdef myModel < handle
    %MODEL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (SetObservable)
        name;
        trainSet;
        testSet;
        %Data set Paths
        trainSetPath;
        testSetPath;
        %Visualize data set
        visualizeDataSetSelector;
        dataSetSize;
        dataSetIndex;
        %Apply reduction
        dataSetNewDimensions;
        dataSetOriginalDimensions;
        reductionMethodSelector;
        %Classifier
        classifierSelector;
        confusionMatrixCheckbox;
        somMapCheckbox;
    end
    
    methods
        function obj = model (name)
            if nargin == 0
                name = 'Carlos Cestari';
            end
            obj.name = name;
        end
    end
    
end

