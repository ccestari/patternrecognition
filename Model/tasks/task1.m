%Results file
module = featureModule;
load Trainnumbers;
load Testnumbers_R13;
trainData = Trainnumbers.image;
testData = Testnumbers.image;
trainLabels = Trainnumbers.label;

%Task 1
    %PCA-28
    disp('Original image PCA-28');
    dimension = 28;
    trainData28 = trainData;
    pcaTrainData28 = trainData;
    MSE28 = 0;
    %PCA-25
    disp('PCA-25');
    dimension = 25;
    [trainData25, pcaTrainData25, MSE25] = module.reduceDimension(trainData, dimension);
    %PCA-20
    disp('PCA-20');
    dimension = 20;
    [trainData20, pcaTrainData20, MSE20] = module.reduceDimension(trainData, dimension);
    %PCA-15
    disp('PCA-15');
    dimension = 15;
    [trainData15, pcaTrainData15, MSE15] = module.reduceDimension(trainData, dimension);
    %PCA-10
    disp('PCA-10');
    dimension = 10;
    [trainData10, pcaTrainData10, MSE10] = module.reduceDimension(trainData, dimension);
    %PCA-5
    disp('PCA-5');
    dimension = 5;
    [trainData5, pcaTrainData5, MSE5] = module.reduceDimension(trainData, dimension);
    %PCA-1
    disp('PCA-1');
    dimension = 1;
    [trainData1, pcaTrainData1, MSE1] = module.reduceDimension(trainData, dimension);