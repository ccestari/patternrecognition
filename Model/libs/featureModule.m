classdef featureModule < handle
    %FEATUREMODULE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        type;
    end
    
    methods
        %Constructor
        function obj = featureModule(type_)
            if nargin == 0
                type_ = 'PCA';
            end
            switch type_
                case 'PCA'
                    obj.type =  type_;
                otherwise
                    error('Unknown feature module type');
            end
        end
        %Get type
        function type_ = getType(obj)
            type_ = obj.type;
        end
        %Dimension reduction
        function [reconstructedImage, pcaCoordinates, mse] = reduceDimension(obj, data, newDimension, normalized)
            %Data type is a mxn matrix, where each column represents a
            %handwritten number of 28x28
            if nargin < 4
                normalized = 0;
            end
            
            type_ = obj.getType;
            reconstructedImage = [];
            pcaCoordinates = [];
            mse = [];
            switch type_
                case 'PCA'
                    disp('Reducing dimensions applying PCA');
                    [reconstructedImage, pcaCoordinates, mse] = obj.PCAanalysis(data, newDimension, normalized);
                otherwise
                    error('Unknown feature module type');
            end
        end
        %PCA analysis
        function [reconstructedImage, pcaCoordinates, mse] = PCAanalysis(obj, data, newDimension, normalized)
            %Data type is a mxn matrix that representes a set of handwritten
            %numbers. By default, m = 28 and n = 10000.
            if nargin < 4
                normalized = 1;
            end
            
            %Calculate covariance matrix
            dataCovariance = obj.covariance(data, normalized);
            %Calculate covariance principal components
            [dataDimension, ~] = size(data);
            if newDimension*newDimension > dataDimension
                error('new dimension must be equal or less than the actual dimension');
            end
            [principalComponents, mse] = obj.getPrincipalComponents(dataCovariance, newDimension);
            %Project data into principal components
            [reconstructedImage, pcaCoordinates] = obj.project(data, principalComponents);
        end
        %Normalize data
        function normalizedData = normalizeData(obj, data)
            normalizedData = [];
        end
    end
    
    methods (Static) %Static methods
        %Covariance matrix
        function dataCov = covariance(data, normalized)
            %Each row is an observation and each column is a variable
            if nargin < 2
                normalized = 0;
            end
            %Calculate covariance matrix between observations around the
            %mean
            if normalized
                dataCov = cov(data', 1);
            else
                dataCov = cov(data');
            end
        end
        %Get principal components
        function [principalComponents, error] = getPrincipalComponents(dataCovariance, newDimension)
            %Take row transVectors
            [eigVectors, eigValues] = eig(dataCovariance); %Each column is an eigenVector
            %Sort eigValues
            eigValues = diag(eigValues);
            [orderedEigValues, order] = sort(eigValues, 'descend');
            %Get principal components
            principalComponents = eigVectors(:, order(1:newDimension*newDimension));
            error = sum(orderedEigValues(1:newDimension*newDimension))/2;
        end
        %Project data into principal components
        function [reconstructedImage, pcaCoordinates] = project(data, principalComponents)
            %Takes original data and projects it into the principal
            %components (vectors)
            [~, newDimensions] = size(principalComponents);
            %Project data into principal components
            %For each handwritten number, project
            pcaCoordinates = [];
            reconstructedImage = zeros(784, 10000);
            for j = 1 : 10000 %total of handwritten numbers
                if mod(j, 100) == 0
                    string = sprintf('Iteration %d', j);
                    disp(string);
                end
                for i = 1 : newDimensions %Would represent nxn dimensions
                    pcaCoordinates(i, j) = dot(data(:, j), principalComponents(:, i)'); %every column of principal component is an eigen vector
                    reconstructedImage(:, j) = (pcaCoordinates(i, j) * principalComponents(:, i) / (norm(principalComponents(:, i)) ^ 2)) + reconstructedImage(:, j);
                end
            end
        end
    end
    
end

