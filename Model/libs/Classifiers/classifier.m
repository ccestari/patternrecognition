classdef classifier < handle
    %LASSIFIER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        type;
        configuration;
        net;
    end
    
    methods
        %Constructor
        function obj = classifier(type_)
            if nargin == 0
                type_ = 'knn';
            end
            obj.setType(type_);
        end
        %Set type
        function setType(obj, type_)
            if obj.isValidType(type_)
                obj.type = type_;
                obj.setUp;
            else
                error('Unknown classifier type');
            end
        end
        %Set up
        function setUp(obj)
            switch obj.type
                case 'knn'
                    configuration_ = struct('kNeighbours', 3, ...
                                    'distance', 'euclidean', ...
                                    'rule', 'nearest');
                case 'bayesian'
                    configuration_ = struct('type', {'linear', 'cuadratic'}, ...
                                    'prior', []);
                case 'mlp'
                    configuration_ = struct('hiddenLayerNeurons', 10, ...
                                        'trainFcn', 'trainscg');                    
                case 'som'
                    configuration_ = struct('hiddenLayerNeurons', 10, ...
                                        'trainFcn', 'trainrp');
            end
            obj.configuration = configuration_;
        end
        %Classify
        function sampleClasses = classify(obj, testData, trainData, trainLabels)
            switch obj.type
                case 'knn'
                    k = obj.configuration.kNeighbours;
                    distance = obj.configuration.distance;
                    rule = obj.configuration.rule;
                    sampleClasses = knnclassify(testData', trainData', trainLabels, k, distance, rule);
                case 'bayesian'
                    type_ = obj.configuration.type;
                    prior = obj.configuration.prior;
                    if isempty(prior)
                        [sampleClasses, ~] = classify(testData', trainData', trainLabels, type_);
                    else
                        [sampleClasses, ~] = classify(testData', trainData', trainLabels, type_, prior);
                    end
                case 'mlp'
                    hiddenLayerNeurons = obj.configuration.hiddenLayerNeurons;
                    offsetLabels = trainLabels + 1;
                    targets = full(ind2vec(offsetLabels));
                    net = patternnet(hiddenLayerNeurons);
                    net = train(net, trainData, targets);
                    output = net(testData);
                    sampleClasses = vec2ind(output) - 1;
                    %Can get performance out of net
                case 'som'
                    hiddenLayerNeurons = obj.configuration.hiddenLayerNeurons;
                    offsetLabels = trainLabels + 1;
                    targets = full(ind2vec(offsetLabels));
                    net = selforgmap(hiddenLayerNeurons);
                    net.trainFcn = 'trainrp';
                    net = configure(net,'outputs', targets);
                    % Train the Network
                    [net,~] = train(net,trainData);
                    % Test the Network
                    output = net(testData);
                    sampleClasses = vec2ind(output) - 1;
                otherwise
                    error('Unknown classifier');
            end
        end
        
       %Train
       function trainedNet = trainClassifier(obj, trainData, trainLabels)
           switch obj.type                
                case 'mlp'
                    hiddenLayerNeurons = obj.configuration.hiddenLayerNeurons;
                    offsetLabels = trainLabels + 1;
                    targets = full(ind2vec(offsetLabels));
                    trainedNet = patternnet(hiddenLayerNeurons);
                    trainedNet = train(trainedNet, trainData, targets);
                    obj.net = traindNet;
                    %Can get performance out of net
                case 'som'
                    hiddenLayerNeurons = obj.configuration.hiddenLayerNeurons;
                    offsetLabels = trainLabels + 1;
                    targets = full(ind2vec(offsetLabels));
                    trainedNet = selforgmap(hiddenLayerNeurons);
                    trainedNet.trainFcn = 'trainrp';
                    trainedNet = configure(trainedNet,'outputs', targets);
                    % Train the Network
                    [trainedNet,~] = train(trainedNet,trainData);
                    obj.net = trainedNet;
                otherwise
                    error('Unknown classifier');
            end
       end
       %Test
       function sampleClasses = classifyTestData(obj, testData)
           switch obj.type
               case 'mlp'
                   output = obj.net(testData);
                   sampleClasses = vec2ind(output) - 1;
               case 'som'
                   output = obj.net(testData);
                   sampleClasses = vec2ind(output) - 1;
               otherwise
                   error('Unknown classifier');
           end
       end
    end
    
    methods (Static)
        %is valid type
        function isValid = isValidType(type_)
            switch type_
                case 'knn'
                    isValid = true;
                case 'bayesian'
                    isValid = true;
                case 'mlp'
                    isValid = true;
                case 'som'
                    isValid = true;
                otherwise
                    isValid = false;
            end
        end
        %Get number of errors
        function numberOfErrors = getNumberOfErrors(classifiedLabels, realLabels)
            numberOfErrors = length(find(classifiedLabels ~= realLabels));
        end
    end
    
end

