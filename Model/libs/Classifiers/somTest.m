%Som test
load Trainnumbers;
inputs = Trainnumbers.image;

targets = label + 1;
targets = full(ind2vec(targets));

%PCA reduction
module = featureModule;
[~, inputs, ~] = module.reduceDimension(inputs, 10);

% Create a Self-Organizing Map
dimension1 = 10;
dimension2 = 10;
net = selforgmap([dimension1 dimension2], 10);
net.trainFcn = 'trainrp';
net = configure(net, 'outputs', targets);

% Train the Network
[net,tr] = train(net,inputs);

% Test the Network
outputs = net(inputs);

% View the Network
view(net)