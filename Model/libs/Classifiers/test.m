load Trainnumbers
mlp = classifier('mlp');
img = Trainnumbers.image;
label = Trainnumbers.label;
offsetlabel = 1 + label;
labelVec = ind2vec(offsetlabel);
target = full(labelVec);

net = patternnet([10, 10]);
net = train(net, img, target);
output = net(img);
outputLabel = vec2ind(output) - 1;
numberOfErrors = mlp.getNumberOfErrors(outputLabel, label);