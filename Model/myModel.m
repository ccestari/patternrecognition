classdef myModel < handle
    %MODEL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties (SetObservable)
        name;
        trainSet;
        testSet;
        %Data set Paths
        trainSetPath;
        testSetPath;
        %Visualize data set
        visualizeDataSetSelector;
        trainSetSize;
        testSetSize;
        dataSetIndex;
        %Apply reduction
        reconstructedTrainData;
        reconstructedTestData;
        trainDataSetNewDimensions;
        testDataSetNewDimensions;
        dataSetOriginalDimensions;
        reductionMethodSelector;
        %Classifier
        classifierSelector;
        confusionMatrixCheckbox;
        somMapCheckbox;
        %Results
        classificationResults;
        confusionMatrix;
        numberOfErrors;
        %Classifiers
        knn;
        bayesian;
        mlp;
        som;
        %Label
        trainLabel;
        classificationLabel;
    end
    
    methods
        %Constructor
        function obj = model (name)
            if nargin == 0
                name = 'Carlos Cestari';
            end
            obj.name = name;
        end
        %Change train set path
        function changeTrainSetPath(obj, newPath)
            if exist(newPath, 'file') ~= 0
                obj.trainSetPath = newPath;
            end
        end
        %Change test set path
        function changeTestSetPath(obj, newPath)
            if exist(newPath, 'file') ~= 0
                obj.testSetPath = newPath;
            end
        end
        %Load train set
        function loadTrainSet(obj)
            load(obj.trainSetPath);
            obj.trainSet = Trainnumbers;
            obj.updateTrainSetRelated;
            obj.reconstructedTrainData = obj.trainSet.image;
            obj.trainDataSetNewDimensions = obj.trainSet.image;
        end
        %Load test set
        function loadTestSet(obj)
            load(obj.testSetPath);
            obj.testSet = Testnumbers;
            obj.updateTestSetRelated;
            obj.reconstructedTestData = obj.testSet.image;
            obj.testDataSetNewDimensions = obj.testSet.image;
        end
        %Update train set related
        function updateTrainSetRelated(obj)
            obj.trainSetSize = size(obj.trainSet.image, 2);
            obj.dataSetOriginalDimensions = sqrt(size(obj.trainSet.image, 1));
            obj.trainLabel = obj.trainSet.label;
        end
        %Update test set related
        function updateTestSetRelated(obj)
            obj.testSetSize = size(obj.testSet.image, 2);
        end
        %Get image
        function img = getImage(obj, trainSelected, index)
            if index < 1
                error('Index must be higher than 0');
            end
            if trainSelected
                if index > size(obj.trainSet.image, 2)
                    error('Index must be valid');
                end
                img = obj.trainSet.image(:, index);
            else
                if index > size(obj.testSet.image, 2)
                    error('Index must be valid');
                end
                img = obj.testSet.image(:, index);
            end
        end
        %Apply reduction
        function applyReduction(obj, newDimensions, trainCheckbox, testCheckbox, handles)
            module = featureModule('PCA');
            if trainCheckbox && (isempty(obj.trainSet) == 0)
                set(handles.applyReduction, 'String', 'Calculating...');
                set(handles.applyReduction, 'Enable', 'off');
                pause(0.5);
                [obj.reconstructedTrainData, obj.trainDataSetNewDimensions, ~] = module.reduceDimension(obj.trainSet.image, newDimensions);                
            end            
            if testCheckbox (isempty(obj.testSet) == 0)
                set(handles.applyReduction, 'String', 'Calculating...');
                set(handles.applyReduction, 'Enable', 'off');
                pause(0.5);
                [obj.reconstructedTestData, obj.testDataSetNewDimensions, ~] = module.reduceDimension(obj.testSet.image, newDimensions);
                obj.classificationLabel = obj.testSet.label;
            end
            pause(0.5);
        end
        %Get reconstructed image
        function img = getReconstructedImage(obj, trainSelected, index)
            if index < 1
                error('Index must be higher than 0');            
            end
            if trainSelected
                if index > size(obj.reconstructedTrainData, 2)
                    error('Index must be valid');
                end
                img = obj.reconstructedTrainData(:, index);
            else
                if index > size(obj.reconstructedTestData, 2)
                    error('Index must be valid');
                end
                img = obj.reconstructedTestData(:, index);
            end
        end
        %Execute classifier
        function executeClassifier(obj, type)
            switch type
                case 'knn'
                    if isempty(obj.knn)
                        obj.knn = classifier('knn');
                    end
                    obj.classificationResults = obj.knn.classify(obj.testDataSetNewDimensions, obj.trainDataSetNewDimensions, obj.trainSet.label);
                    obj.classificationResults = obj.classificationResults';
                case 'bayesian'
                    if isempty(obj.bayesian)
                        obj.bayesian = classifier('bayesian');
                    end
                    obj.classificationResults = obj.bayesian.classify(obj.testDataSetNewDimensions, obj.trainDataSetNewDimensions, obj.trainSet.label);
                    obj.classificationResults = obj.classificationResults';
                case 'mlp'
                    if isempty(obj.mlp)
                        obj.mlp = classifier('mlp');
                    end
                    obj.classificationResults = obj.mlp.classify(obj.testDataSetNewDimensions, obj.trainDataSetNewDimensions, obj.trainSet.label);
                case 'som'
                    if isempty(obj.som)
                        obj.som = classifier('som');                        
                    end
                    obj.classificationResults = obj.som.classify(obj.testDataSetNewDimensions, obj.trainDataSetNewDimensions, obj.trainSet.label);
                otherwise
                    error('Unknown classifier');
            end
            obj.numberOfErrors = classifier.getNumberOfErrors(obj.classificationResults, obj.trainSet.label);
            obj.confusionMatrix = confusionmat(obj.trainSet.label, obj.classificationResults);
            disp('done');
        end
        %Get confusion matrix
        function confusionMatrix = getConfusionMatrix(obj)
            confusionMatrix = obj.confusionMatrix;
        end
        %Get number of errors
        function numberOfErrors = getNumberOfErrors(obj)
            numberOfErrors = obj.numberOfErrors;
        end
        %Update index
        function updateIndex(obj, index)
            obj.dataSetIndex = index;
        end
    end
end

