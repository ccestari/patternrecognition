%SOM
load Trainnumbers.mat;
load Testnumbers_R13.mat;
load trainData28.mat;
load testData.mat;
load pcaTrainData25.mat
load pcaTestData25.mat
load pcaTrainData15.mat
load pcaTestData15.mat
load pcaTrainData10.mat
load pcaTestData10.mat
label = Trainnumbers.label;

som = classifier('som');
%PCA15
classesPCA15_10_som = som.classify(pcaTestData15, pcaTrainData15, label);