%MLP
load Trainnumbers.mat;
load Testnumbers_R13.mat;
load trainData28.mat;
load testData.mat;
load pcaTrainData25.mat
load pcaTestData25.mat
load pcaTrainData15.mat
load pcaTestData15.mat
load pcaTrainData10.mat
load pcaTestData10.mat
label = Trainnumbers.label;

mlp = classifier('mlp');
%PCA25 10
hiddenLayer = 15;
classPCA25_10_mlp = mlp.classify(pcaTestData25, pcaTrainData25, label);
%PCA15 10
hiddenLayer = 10;
classPCA15_10_mlp = mlp.classify(pcaTestData15, pcaTrainData15, label);
%PCA10 10
hiddenLayer = 10;
classPCA10_10_mlp = mlp.classify(pcaTestData10, pcaTrainData10, label);
