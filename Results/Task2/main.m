clear all
close all
clc
%Knn classifier
load Trainnumbers.mat
load Testnumbers_R13.mat
trainData = Trainnumbers.image;
label = Trainnumbers.label;
testData = Testnumbers.image;
module = featureModule;
knn = classifier('knn');

%Run PCA over Test
    %PCA25
    dimension = 25;
    [testData25, pcaTestData25, ~] = module.reduceDimension(testData, dimension);
    %PCA15
    dimension = 15;
    [testData15, pcaTestData15, ~] = module.reduceDimension(testData, dimension);
    %PCA10
    dimension = 10;
    [testData10, pcaTestData10, ~] = module.reduceDimension(testData, dimension);
    
%Classify with knn
    %K = 3 PCA28
    classesPCA28_K3 = knn.classify(testData, trainData, label);
   
    %K = 3 PCA25
    load pcaTrainData25.mat;
    classesPCA25_K3 = knn.classify(pcaTestData25, pcaTrainData25, label);
    
    %K = 3 PCA15
    load pcaTrainData15.mat;
    classesPCA15_K3 = knn.classify(pcaTestData15, pcaTrainData15, label);
    
    %K = 3 PCA10
    load pcaTrainData10.mat;
    classesPCA25_10 = knn.classify(pcaTestData10, pcaTrainData10, label);