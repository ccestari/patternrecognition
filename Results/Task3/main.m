clear all;
clc;
close all;

load Trainnumbers.mat;
label = Trainnumbers.label;
load Testnumbers_R13.mat;
bay = classifier('bayesian');

%Bayesian
%PCA28
% load trainData28.mat
% load testData.mat
% classesPCA28_bay = bay.classify(testData, trainData28, label);

%PCA25
load pcaTrainData25.mat
load pcaTestData25.mat
classesPCA25_bay = bay.classify(pcaTestData25, pcaTrainData25, label);

%PCA15
load pcaTrainData15.mat
load pcaTestData15.mat
classesPCA15_bay = bay.classify(pcaTestData15, pcaTrainData15, label);

%PCA10
load pcaTrainData10.mat
load pcaTestData10.mat
classesPCA10_bay = bay.classify(pcaTestData10, pcaTrainData10, label);

