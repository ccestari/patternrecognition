classdef myController < handle
    %MYCONTROLLER Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        model;
        view;
    end
    
    methods
        %Constructor
        function obj = myController(model)
            if nargin == 0
                model = myModel;
            end
            obj.model = model;
            obj.view = myView(obj);
        end
        %Change train set path
        function changeTrainSetPath(obj, newPath)
            obj.model.changeTrainSetPath(newPath);
        end
        %Change test set path
        function changeTestSetPath(obj, newPath)
            obj.model.changeTestSetPath(newPath);
        end
        %Load train set
        function loadTrainSet(obj)
            obj.model.loadTrainSet;
        end
        %Load test set
        function loadTestSet(obj)
            obj.model.loadTestSet;
        end
        %Load test set
        function visualizeImage(obj, handles)
            trainSelected = get(handles.visualizeTrainSelector, 'Value');
            index = str2num(get(handles.dataSetIndex, 'String'));
            img = obj.model.getImage(trainSelected, index);
            axes(handles.image);
            img = reshape(img, 28, 28);
            image(img);
        end
        %Apply reduction
        function applyReduction(obj, newDimensions, trainSetCheckbox, testSetCheckbox)
            handles = guidata(obj.view.mainGui);
            obj.model.applyReduction(newDimensions, trainSetCheckbox, testSetCheckbox, handles);            
        end
        %Show reconstructed image
        function showReconstructedImage(obj, handles)
            trainSelected = get(handles.visualizeTrainSelector, 'Value');
            index = str2num(get(handles.dataSetIndex, 'String'));
            img = obj.model.getReconstructedImage(trainSelected, index);
            axes(handles.image);
            if isempty(img)
                error('image hasnt been initialized');
            else
                img = reshape(img, 28, 28);
                image(img);
            end
        end
        %Execute classifier
        function executeClassifier(obj, selectorType)
            obj.model.executeClassifier(selectorType)
        end
        %Show results
        function showResults(obj)
            handles = guidata(obj.view.mainGui);
            confusionMatrixSelector = get(handles.confusionMatrixCheckbox, 'Value');
            numberOfErrorsSelector = get(handles.numberOfErrorsCheckbox, 'Value');
            somMapSelector = get(handles.somMapCheckbox, 'Value');
            results = figure;
            if confusionMatrixSelector
                confusionMatrix = obj.model.getConfusionMatrix;                
                figurePosition = get(results, 'Position');
                set(results, 'Position', [figurePosition(1) - 200, figurePosition(2), figurePosition(3) + 200, figurePosition(4)]);
                postable = uitable('Data', confusionMatrix);                
                columnWidth = ones(1, max(obj.model.trainSet.label) + 1)*30;
                columnWidth = num2cell(columnWidth);
                set(postable, 'ColumnWidth', columnWidth);
                tablePosition = get(postable, 'Position');
                tableExtent = get(postable, 'Extent');               
                set(postable, 'Position', [tablePosition(1), tablePosition(2)- tableExtent(4) + 400, tableExtent(3:4)]);
            end
            if numberOfErrorsSelector
                numberOfErrors = obj.model.getNumberOfErrors;
                uicontrol('style', 'text', 'String', num2str(numberOfErrors));
            end
            if somMapSelector
                disp('This does nothing');
            end
        end
        %Update index
        function updateIndex(obj, index)
            obj.model.updateIndex(index);
        end
    end
end
